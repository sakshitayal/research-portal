import { handleResponse, handleError } from "./apiUtils";
const baseUrl = "http://localhost:3001/variables/";

export function getvariables() {
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}
