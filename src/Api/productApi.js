import { handleResponse, handleError } from "./apiUtils";
const baseUrl = "http://localhost:3001/dataProducts/";

export function getProducts() {
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}

export function saveProduct(data) {
  return fetch(baseUrl + (data.id || ""), {
    method: data.id ? "PUT" : "POST", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify(data)
  })
    .then(handleResponse)
    .catch(handleError);
}

export function deleteProduct(dataId) {
  return fetch(baseUrl + dataId, { method: "DELETE" })
    .then(handleResponse)
    .catch(handleError);
}
