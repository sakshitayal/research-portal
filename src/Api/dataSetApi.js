import { handleResponse, handleError } from "./apiUtils";
const baseUrl = "http://localhost:3001/dataSets/";
const documentBaseUrl = "http://localhost:3001/documents/";

export function getdataSets() {
  return fetch(baseUrl).then(handleResponse).catch(handleError);
}

export function getdocuments() {
  return fetch(documentBaseUrl).then(handleResponse).catch(handleError);
}
