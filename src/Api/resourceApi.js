import { handleResponse, handleError } from "./apiUtils";
const baseUrl = "http://localhost:3001/dataResources/";

export function getResources() {
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}
