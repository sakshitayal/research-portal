import { uuid } from "./utils";

export const generateNotification = (level, title, body, errorCode) => {
  return {
    id: uuid(),
    level,
    title: errorCode ? `${errorCode}: ${title}` : title,
    body,
  };
};

export const NOTIFICATION_TYPES = {
  ERROR: "error",
  SUCCESS: "success",
};
