import moment from "moment";

export const formatDate = (date, format) => {
  if (date instanceof Date) {
    date = date.toISOString();
  }

  const momentDate = moment(date);

  return date && momentDate.isValid()
    ? momentDate.format(format || "MMM Do YYYY")
    : null;
};

export const uuid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    // eslint-disable-next-line
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

export const cognitoPasswordRegEx = () => {
  return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,99}$/;
};
export const mobileNoRegEx = () => {
  return /^(\+\d{1,3}[- ]?)?\d{10}$/;
};
export const getOrigin = () => {
  return window.location.origin;
};

export const setLocalStorage = (key, value) => {
  localStorage.setItem(key, value && JSON.stringify(value));
};

export const removeLocalStorage = (key) => {
  localStorage.removeItem(key);
};

export const getLocalStorage = (key) => {
  return key && localStorage.getItem(key)
    ? JSON.parse(localStorage.getItem(key))
    : "";
};

export const findSelectableAncestor = (domElement, depth, className) => {
  if (domElement.classList.contains(className)) {
    return domElement;
  } else {
    if (domElement.parentElement && depth) {
      return findSelectableAncestor(
        domElement.parentElement,
        depth - 1,
        className
      );
    } else {
      return undefined;
    }
  }
};
