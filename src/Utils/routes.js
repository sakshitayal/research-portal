export const ROUTES = {
  LOGIN: "/login",
  LANDING: "/landing",
  ROOT: "/",
  DATA: "/library/Data",
  DOCUMENTS: "/library/Documents",
  DASHBOARD: "/workspace/Dashboard",
  DATAPRODUCTBUILDER: "/workspace/DataProductBuilder",
  DATARESOURCES: "/workspace/DataResources",
  PROJECTS: "/workspace/Projects",
  BUILDDATAPRODUCT1: "/workspace/BuildDataProduct1",
  NEWPROJECT: "/workspace/NewProject",
  NEWPASSWORD: "/newPassword",
  SETMFA: "/SetMFA",
};
