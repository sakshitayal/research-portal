import Amplify, { Auth } from "aws-amplify";
import { prod } from "./production";
import { dev } from "./development";

export const configuration =
  process.env.REACT_APP_NOT_SECRET_CODE === "production" ? prod : dev;

const aws_oauth = {
  scope: ["profile email openid aws.cognito.signin.user.admin"],
  responseType: "code",
  options: {
    AdvancedSecurityDataCollectionFlag: false,
    forceAliasCreation: false,
  },
};

class Authentication {
  constructor() {
    Amplify.configure({
      Auth: {
        mandatorySignIn: true,
        region: configuration.cognito.aws_region,
        userPoolId: configuration.cognito.aws_userPoolId,
        identityPoolId: configuration.cognito.aws_identity_pool_id,
        userPoolWebClientId: configuration.cognito.aws_app_clientId,
        oauth: aws_oauth,
      },
    });
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.getCurrentUser = this.getCurrentUser.bind(this);
    this.completeNewPassword = this.completeNewPassword.bind(this);
    this.addTTOP = this.addTTOP.bind(this);
    this.verifyTotpToken = this.verifyTotpToken.bind(this);
    this.confirmSignIn = this.confirmSignIn.bind(this);
    this.setPreferredMFA = this.setPreferredMFA.bind(this);
  }

  async getCurrentUser() {
    try {
      let user = await Auth.currentAuthenticatedUser();
      return user;
    } catch (err) {
      throw err;
    }
  }

  async login(userDetails) {
    let { username, password } = userDetails;
    username = username.toLowerCase();
    try {
      let signedIn = await Auth.signIn(username, password);
      return signedIn;
    } catch (err) {
      throw err;
    }
  }

  async logout() {
    try {
      await Auth.signOut();
      return true;
    } catch (err) {
      throw err;
    }
  }

  async forgotPassword(userDetails) {
    try {
      let { email } = userDetails;
      email = email.toLowerCase();
      const result = await Auth.forgotPassword(email);
      return result;
    } catch (err) {
      throw err;
    }
  }

  async resetPassword(payload) {
    try {
      let { email, verificationCode, password } = payload;
      email = email.toLowerCase();
      const result = await Auth.forgotPasswordSubmit(
        email,
        verificationCode,
        password
      );
      return result;
    } catch (err) {
      throw err;
    }
  }

  async completeNewPassword(payload) {
    try {
      let { username, password, newPassword } = payload;
      const user = await Auth.signIn(username, password);
      const result = await Auth.completeNewPassword(user, newPassword, {
        email: username,
        //phone_number: "+447435853058",
      });
      return result;
    } catch (err) {
      throw err;
    }
  }

  async confirmSignIn(user, challengeAnswer) {
    try {
      let data = await Auth.confirmSignIn(
        user,
        challengeAnswer,
        user.challengeName
      );
      return data;
    } catch (err) {
      throw err;
    }
  }

  async addTTOP(user) {
    try {
      let authCode = "";
      await Auth.setupTOTP(user).then((code) => {
        authCode =
          "otpauth://totp/AWSCognito:" +
          user.username +
          "?secret=" +
          code +
          "&issuer=AWSCognito";
      });

      return authCode;
    } catch (err) {
      throw err;
    }
  }

  async verifyTotpToken(user, challengeAnswer) {
    try {
      let data = await Auth.verifyTotpToken(user, challengeAnswer);
      return data;
    } catch (err) {
      throw err;
    }
  }

  async setPreferredMFA(user) {
    try {
      await Auth.setPreferredMFA(user, "TOTP").then((data) => {
        return data;
      });
    } catch (err) {
      throw err;
    }
  }
}

const authentication = new Authentication();
export { Authentication };
export default authentication;
