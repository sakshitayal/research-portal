import { connectedRouterRedirect } from "redux-auth-wrapper/history4/redirect";
import locationHelperBuilder from "redux-auth-wrapper/history4/locationHelper";

import { getLocalStorage, removeLocalStorage } from "./utils";
//import Loader from "../components/Loader/Loader";
import { ROUTES } from "./routes";

export const USER_TYPES = {
  CLIENT: "CLIENT",
  DISTRIBUTER: "DISTRIBUTER",
};

const locationHelper = locationHelperBuilder({});

export const userIsAuthenticatedRedir = connectedRouterRedirect({
  authenticatedSelector: () => {
    const user = getLocalStorage("user");
    return user && !!user.token;
  },
  wrapperDisplayName: "UserIsAuthenticated",
  //AuthenticatingComponent: Loader,
  redirectPath: ROUTES.LOGIN,
});

export const userIsNotAuthenticatedRedir = connectedRouterRedirect({
  authenticatedSelector: () => {
    const user = getLocalStorage("user");
    return !user;
  },
  wrapperDisplayName: "UserIsNotAuthenticated",
  redirectPath: (state, ownProps) =>
    locationHelper.getRedirectQueryParam(ownProps) ||
    ownProps.location.pathname,
});

export const logoutUser = () => {
  removeLocalStorage("user");
};

export const flattenUserObject = (user) => {
  const userDetails = user.user_id;

  let newUserObj = {
    ...userDetails,
    ...user,
  };

  delete newUserObj.user_id;

  return newUserObj;
};
