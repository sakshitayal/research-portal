import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Icon from "@material-ui/core/Icon";
import PropTypes from "prop-types";

const StyledTableCell = withStyles(() => ({
  head: {
    fontSize: 16,
    fontWeight: 700,
  },
  body: {
    fontSize: 15,
  },
}))(TableCell);

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 700,
  },
}));

export default function CustomTable({
  columns,
  rows,
  rowsPerPageArr,
  onRowClick,
}) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(rowsPerPageArr[0]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const getformattedValue = (column, value) => {
    if (column.format && typeof value === "number") return column.format(value);
    else if (column.icon) {
      switch (value) {
        case "approved":
          return (
            <span>
              <Icon style={{ color: "green" }}>check_circle</Icon>
            </span>
          );
        case "partiallyApproved":
          return (
            <span>
              <Icon style={{ color: "orange" }}>check_circle</Icon>
            </span>
          );
        case "declined":
          return (
            <span>
              <Icon style={{ color: "red" }}>cancel</Icon>
            </span>
          );
        case "pending":
          return (
            <span>
              <Icon style={{ color: "orange" }}>hourglass_empty</Icon>
            </span>
          );
        case "notSubmitted":
          return (
            <span>
              <Icon style={{ color: "black" }}>help_outline</Icon>
            </span>
          );
      }
    } else return value;
  };

  return (
    <div>
      {rows.length !== 0 ? (
        <Paper>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <StyledTableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </StyledTableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    return (
                      <TableRow
                        hover
                        key={index}
                        onClick={(event) => onRowClick(event, index)}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <StyledTableCell
                              key={column.id}
                              align={column.align}
                            >
                              {getformattedValue(column, value)}
                            </StyledTableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={rowsPerPageArr}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
      ) : (
        ""
      )}
    </div>
  );
}

CustomTable.propTypes = {
  rows: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
};
