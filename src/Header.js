import React from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Icon from "@material-ui/core/Icon";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";

import logoUrl from "./assets/images/logo.png";
import SidebarMenu from "./SidebarMenu";
import { getLocalStorage } from "./Utils/utils";
import { ROUTES } from "./Utils/routes";
import { userLogout } from "./Redux/actions/userActions";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "black",
  },
  toolbar: theme.mixins.toolbar,
  loginLink: {
    float: "right",
    marginTop: 14,
  },
  loginAnchor: {
    textDecoration: "none",
    fontSize: 20,
    fontWeight: 700,
    color: "white",
    textTransform: "initial",
  },
  loginIcon: {
    fontSize: "2em",
  },
  loginSpan: {
    verticalAlign: "super",
  },
  headerLogo: {
    float: "left",
    margin: 5,
  },
  displayBlock: { display: "block" },
}));

function Header({ userLogout, history, ...props }) {
  const classes = useStyles();
  const isUserLoggedIn = () => {
    const user = getLocalStorage("user");
    return user && !!user.token && new Date().getTime() < user.authExpireTime;
  };

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (event, shouldLogout) => {
    event.preventDefault();
    if (shouldLogout) {
      userLogout();
    }
    setOpen(false);
  };

  return (
    <>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.displayBlock}>
          <div className={classes.headerLogo}>
            <Link to={isUserLoggedIn() ? ROUTES.LANDING : ""}>
              <img
                src={logoUrl}
                className="smrp-header-logo"
                alt="SERL Researcher Portal"
              />
            </Link>
          </div>

          <div className={classes.loginLink}>
            {!isUserLoggedIn() && (
              <Link to={ROUTES.LOGIN} className={classes.loginAnchor}>
                <Icon className={classes.loginIcon}>person_outline</Icon>
                <span className={classes.loginSpan}>Login</span>
              </Link>
            )}

            {isUserLoggedIn() && (
              <Button onClick={handleClickOpen} className={classes.loginAnchor}>
                <Icon className={classes.loginIcon}>person_outline</Icon>
                <span className={classes.loginSpan}>Logout</span>
              </Button>
            )}
          </div>
        </Toolbar>
      </AppBar>
      {isUserLoggedIn() && <SidebarMenu />}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Log out of Researcher portal
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            You are about to be logged out.
            <br />
            Are you sure?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" name="cancel">
            Cancel
          </Button>
          <Button
            onClick={(e) => handleClose(e, true)}
            color="primary"
            name="logout"
          >
            Log out
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

Header.propTypes = {
  userLogout: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  userLogout,
};

export default withRouter(connect(null, mapDispatchToProps)(Header));
