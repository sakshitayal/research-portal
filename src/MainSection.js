import React from "react";
import Typography from "@material-ui/core/Typography";

function MainSection() {
  return (
    <div style={{ padding: "25px" }}>
      <Typography paragraph>About SMRP</Typography>
      <Typography paragraph>Getting started</Typography>
      <Typography paragraph>Site map and terminology</Typography>
      <Typography paragraph>Glossary</Typography>
      <Typography paragraph>SMRP documentation</Typography>
      <Typography paragraph>
        Quick links to publicaly available datasets
      </Typography>
      <Typography paragraph>Privacy statments</Typography>
    </div>
  );
}

export default MainSection;
