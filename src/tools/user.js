import { USER_TYPES } from "../Utils/user";

export class User {
  static MOCK = new User();

  constructor(
    user = {
      _id: "test-id",
      created_at: "2019-01-03T09:17:26.062Z",
      email: "test@test.com",
      is_active: true,
      type: USER_TYPES.CLIENT,
      user_name: "testuser",
    }
  ) {
    this.user = user;
  }
}
