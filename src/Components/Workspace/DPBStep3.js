import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Radio from "@material-ui/core/Radio";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750
  },
  heading: {
    float: "left"
  },
  rightHeading: {
    float: "right",
    marginBottom: 10
  },
  clearFloat: {
    clear: "both"
  }
}));

const headCells = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Linked Dataset"
  },
  {
    id: "LinkageKey",
    numeric: false,
    disablePadding: true,
    label: "Linkage Key"
  },
  {
    id: "headerRadioButton",
    numeric: false,
    disablePadding: true,
    label: "Select"
  }
];

export default function DPBStep3({ dataSets, product, onChange, errors = {} }) {
  const classes = useStyles();
  const [selected, setSelected] = React.useState(product.dataSet);

  const handleChange = event => {
    setSelected(event.target.value);

    onChange(event);
  };

  return (
    <div>
      <div>
        <Typography variant="h5" className={classes.heading}>
          Choose a primary dataset to work with:
        </Typography>
        <div className={classes.rightHeading}>
          <Typography variant="h6">Case count: 15,622</Typography>
          <Typography variant="h6">Variable count: 79</Typography>
        </div>
      </div>
      <div className={classes.clearFloat}></div>

      {dataSets !== undefined && dataSets.length >= 0 ? (
        <Paper className={classes.paper}>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={"medium"}
            >
              <TableHead>
                <TableRow>
                  {headCells.map(headCell => (
                    <TableCell
                      key={headCell.id}
                      align={"left"}
                      padding={headCell.padding ? "none" : "default"}
                    >
                      {headCell.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {dataSets.map((row, index) => {
                  return (
                    <TableRow
                      hover
                      role="Radiobox"
                      tabIndex={-1}
                      key={row.name}
                    >
                      <TableCell>{row.name}</TableCell>
                      <TableCell>{row.LinkageKey}</TableCell>
                      <TableCell padding="checkbox">
                        <Radio
                          checked={selected === row.name}
                          onChange={handleChange}
                          value={row.name}
                          name="dataSet"
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      ) : (
        ""
      )}
    </div>
  );
}

DPBStep3.propTypes = {
  dataSets: PropTypes.array.isRequired,
  errors: PropTypes.object,
  onChange: PropTypes.func.isRequired
};
