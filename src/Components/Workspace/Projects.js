import React, { useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import * as projectActions from "../../Redux/actions/projectAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { Link, withRouter } from "react-router-dom";
import CustomTable from "../../Common/table";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Spinner from "../../Common/Spinner";

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  summaryText: { verticalAlign: "super" },
  heading: {
    float: "left",
  },
  button: {
    float: "right",
    marginBottom: 10,
  },
  clearFloat: {
    clear: "both",
  },
}));

const columns = [
  { id: "id", label: "ID" },
  { id: "title", label: "Title" },
  { id: "approvalStatus", label: "Approval Status", icon: true },
  { id: "secureLab", label: "secure Lab" },
  { id: "owner", label: "Owner" },
  { id: "expires", label: "Expires" },
];

function Projects(props) {
  const classes = useStyles();
  const { projects, actions } = props;

  useEffect(() => {
    if (projects.length === 0) {
      actions.loadProjects().catch((error) => {
        alert("Loading projects failed" + error);
      });
    }
  }, []);

  return (
    <div style={{ padding: "25px" }}>
      <div>
        <Typography variant="h5" className={classes.heading}>
          Projects
        </Typography>
        <Link to="./NewProject">
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            startIcon={<Icon>playlist_add_check</Icon>}
          >
            New Project
          </Button>
        </Link>
      </div>
      <div className={classes.clearFloat}></div>
      {props.loading ? (
        <Spinner />
      ) : (
        <CustomTable
          columns={columns}
          rows={projects}
          rowsPerPageArr={[5, 10, 15]}
        />
      )}

      <br />
      <div>
        <div>
          <Icon style={{ color: "green" }}>check_circle</Icon>&nbsp;
          <span className={classes.summaryText}>
            Project is approved and no additional/changes are outstanding. All
            data in the project can be accessed.
          </span>
        </div>

        <div>
          <Icon style={{ color: "orange" }}>check_circle</Icon>&nbsp;
          <span className={classes.summaryText}>
            Project is approved and data can be accessed but spme changes (e.g.
            additional datasets) are pending approval.
          </span>
        </div>

        <div>
          <Icon style={{ color: "orange" }}>hourglass_empty</Icon>&nbsp;
          <span className={classes.summaryText}>
            Project is pending approval. Data in the project is not available
            for access yet.
          </span>
        </div>

        <div>
          <Icon style={{ color: "red" }}>cancel</Icon>&nbsp;
          <span className={classes.summaryText}>
            Project has been denied approval. No data can be accessed in the
            context of this project.
          </span>
        </div>

        <div>
          <Icon style={{ color: "black" }}>help_outline</Icon>&nbsp;
          <span className={classes.summaryText}>
            Project has not been submitted for approval yet.
          </span>
        </div>
      </div>
    </div>
  );
}

Projects.propTypes = {
  projects: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    projects: state.projects,
    loading: state.apiCallsInProgress > 0,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadProjects: bindActionCreators(projectActions.loadProjects, dispatch),
    },
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Projects)
);
