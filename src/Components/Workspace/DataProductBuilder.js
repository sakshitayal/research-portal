import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import * as productActions from "../../Redux/actions/productAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { Link, withRouter } from "react-router-dom";
import CustomTable from "../../Common/table";

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  heading: {
    float: "left",
  },
  button: {
    float: "right",
    marginBottom: 10,
  },
  clearFloat: {
    clear: "both",
  },
}));

const columns = [
  { id: "title", label: "Title" },
  { id: "buildStatus", label: "Build Status" },
  { id: "inProject", label: "In Project" },
];

function DataProductBuilder(props) {
  const classes = useStyles();
  const { products, actions } = props;

  useEffect(() => {
    if (products.length === 0) {
      actions.loadProducts().catch((error) => {
        alert("Loading products failed" + error);
      });
    }
  }, []);

  return (
    <div style={{ padding: "25px" }}>
      <div>
        <Typography variant="h5" className={classes.heading}>
          Data Products
        </Typography>
        <Link to="./BuildDataProduct1">
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Create new data product
          </Button>
        </Link>
      </div>
      <div className={classes.clearFloat}></div>
      <CustomTable columns={columns} rows={products} />
    </div>
  );
}

DataProductBuilder.propTypes = {
  products: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    products: state.products,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadProducts: bindActionCreators(productActions.loadProducts, dispatch),
    },
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(DataProductBuilder)
);
