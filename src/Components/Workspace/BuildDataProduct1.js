import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import DPBStep1 from "./DPBStep1";
import DPBStep2 from "./DPBStep2";
import DPBStep3 from "./DPBStep3";
import DPBStep4 from "./DPBStep4";

import { connect } from "react-redux";
import { loadResources } from "../../Redux/actions/resourceAction";
import { loadDataSets } from "../../Redux/actions/dataSetAction";
import { loadVariables } from "../../Redux/actions/variableAction";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { saveProduct } from "../../Redux/actions/productAction";
import { newProduct } from "../../tools/mockData";

const useStyles = makeStyles((theme) => ({
  heading: {
    float: "left",
  },
  button: {
    float: "right",
    marginBottom: 10,
  },
  clearFloat: {
    clear: "both",
  },
  root: {
    width: "100%",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    "Select resource",
    "refine coverage",
    "Identify linkages",
    "Filter",
    "Output variables",
    "Build",
  ];
}

function BuildDataProduct1({
  loadResources,
  loadDataSets,
  loadVariables,
  saveProduct,
  ...props
}) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [product, setProduct] = React.useState({ ...props.product });
  const [errors, setErrors] = useState({});

  const { resources, dataSets, variables } = props;

  useEffect(() => {
    if (resources.length === 0) {
      loadResources().catch((error) => {
        alert("Loading resources failed" + error);
      });
    }

    if (dataSets.length === 0) {
      loadDataSets().catch((error) => {
        alert("Loading dataSets failed" + error);
      });
    }

    if (variables.length === 0) {
      loadVariables().catch((error) => {
        alert("Loading variables failed" + error);
      });
    }
  }, []);

  const steps = getSteps();
  const components = {
    step1: DPBStep1,
    step2: DPBStep2,
    step3: DPBStep3,
    step4: DPBStep4,
  };

  const getDPBStep = () => {
    const TagName = components[`step${activeStep + 1}`];

    switch (activeStep) {
      case 0:
        return (
          <TagName
            resources={resources}
            onChange={handleChange}
            errors={errors}
            product={product}
          />
        );
      case 1:
        return <TagName />;
      case 2:
        return (
          <TagName
            dataSets={dataSets}
            onChange={handleChange}
            errors={errors}
            product={product}
          />
        );
      case 3:
        return (
          <TagName variables={variables} errors={errors} product={product} />
        );
      case 4:
        return (
          <TagName variables={variables} errors={errors} product={product} />
        );
      case 5:
        return (
          <TagName variables={variables} errors={errors} product={product} />
        );
      default:
        return <TagName />;
    }
  };

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  function handleChange(event, val) {
    const { name, value } = event.target;
    setProduct((prevProduct) => ({
      ...prevProduct,
      [name]: value.length > 0 ? value : val,
    }));
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    saveProduct(product).then(() => {});
  };

  return (
    <div style={{ padding: "25px" }}>
      <div>
        <Typography variant="h5" className={classes.heading}>
          Build new data product
        </Typography>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          type="submit"
          form="product-form"
        >
          Save progress
        </Button>
      </div>
      <div className={classes.clearFloat}></div>

      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed
              </Typography>
              <Button onClick={handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Back
              </Button>
              {activeStep === steps.length - 3 ? (
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.backButton}
                >
                  Link to more data
                </Button>
              ) : (
                ""
              )}
              <Button variant="contained" color="primary" onClick={handleNext}>
                {activeStep === steps.length - 1 ? "Finish" : "Next"}
              </Button>
            </div>
          )}
        </div>
      </div>
      <hr />

      <form onSubmit={handleSubmit} id="product-form">
        {errors.onSave && (
          <div className="alert alert-danger" role="alert">
            {errors.onSave}
          </div>
        )}

        <div id="DPBStep">{getDPBStep()}</div>
      </form>
    </div>
  );
}

BuildDataProduct1.propTypes = {
  resources: PropTypes.array.isRequired,
  dataSets: PropTypes.array.isRequired,
  variables: PropTypes.array.isRequired,
  saveProduct: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  product: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    resources: state.resources,
    dataSets: state.dataSets,
    variables: state.variables,
    product: newProduct,
  };
}

const mapDispatchToProps = {
  loadResources,
  loadDataSets,
  loadVariables,
  saveProduct,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(BuildDataProduct1)
);
