import React from "react";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

export default function DPBStep1() {
  return (
    <div>
      <Typography variant="h5">
        Choose a primary dataset to work with:
      </Typography>

      <Typography variant="h5">
        Give a working title for this data product
      </Typography>
      <TextField id="outlined-basic" label="title" variant="outlined" />
    </div>
  );
}
