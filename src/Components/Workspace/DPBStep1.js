import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750
  }
}));

const headCells = [
  {
    id: "headerCheckbox",
    numeric: false,
    disablePadding: true,
    label: ""
  },
  {
    id: "dataResource",
    numeric: false,
    disablePadding: true,
    label: "Data Resource"
  },
  { id: "study", numeric: false, disablePadding: false, label: "Study" },
  { id: "cases", numeric: true, disablePadding: false, label: "Cases" },
  { id: "variables", numeric: true, disablePadding: false, label: "Variables" }
];

export default function DPBStep1({
  resources,
  product,
  onChange,
  errors = {}
}) {
  const classes = useStyles();
  const [selected, setSelected] = React.useState(product.dataResource);

  const handleClick = (event, name) => {
    selected === name ? setSelected("") : setSelected(name);

    onChange(event, name);
  };

  return (
    <div>
      <Typography variant="h5">
        Choose a primary dataset to work with:
      </Typography>

      {resources !== undefined && resources.length >= 0 ? (
        <Paper className={classes.paper}>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={"medium"}
              aria-label="enhanced table"
            >
              <TableHead>
                <TableRow>
                  {headCells.map(headCell => (
                    <TableCell
                      key={headCell.id}
                      align={"left"}
                      padding={headCell.disablePadding ? "none" : "default"}
                    >
                      {headCell.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {resources.map((row, index) => {
                  return (
                    <TableRow hover key={row.dataResource}>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={selected === row.dataResource}
                          name="dataResource"
                          onClick={e => handleClick(e, row.dataResource)}
                        />
                      </TableCell>
                      <TableCell>{row.dataResource}</TableCell>
                      <TableCell>{row.study}</TableCell>
                      <TableCell>{row.cases}</TableCell>
                      <TableCell>{row.variables}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      ) : (
        ""
      )}
      <Typography variant="h6">
        Give a working title for this data product
      </Typography>
      <TextField
        id="outlined-basic"
        label="title"
        variant="outlined"
        onChange={onChange}
        error={errors.title}
        value={product.title}
        name="title"
      />
      <div>
        <i>You can change this later</i>
      </div>
    </div>
  );
}

DPBStep1.propTypes = {
  resources: PropTypes.array.isRequired,
  errors: PropTypes.object,
  onChange: PropTypes.func.isRequired
};
