import React, { useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { withStyles, makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";
import RemoveCircleOutlineOutlinedIcon from "@material-ui/icons/RemoveCircleOutlineOutlined";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Chip from "@material-ui/core/Chip";
import { saveProject } from "../../Redux/actions/projectAction";
import { newProject, softwaresList, dataSetList } from "../../tools/mockData";

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "rgba(47, 46, 46, 0.54)",
    border: "1px solid black",
    fontSize: 16,
    textAlign: "left",
    padding: 10,
  },
  body: {
    fontSize: 14,
    border: "1px solid black",
    padding: 0,
  },
}))(TableCell);

const useStyles = makeStyles((theme) => ({
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
  container: {
    maxHeight: 440,
  },
  expansionPanelDetails: {
    display: "inherit",
  },
  heading: {
    float: "left",
  },
  expansionPanelHeading: {
    backgroundColor: "#e0e0e0",
    margin: 0,
  },
  button: {
    float: "right",
    marginBottom: 10,
    marginRight: 10,
  },
  clearFloat: {
    clear: "both",
  },
  root: {
    width: "100%",
  },
  childSection: {
    marginLeft: 15,
  },
  defaultWidth: {
    width: "-webkit-fill-available",
  },
  dateTextField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  defaultMargin: {
    marginLeft: 100,
    display: "inline-block",
  },
  formLabel: {
    margin: 20,
    fontWeight: 700,
  },
}));

function NewProject({ saveProject, history, ...props }) {
  const classes = useStyles();
  const [project, setProject] = React.useState({ ...props.project });
  const [errors, setErrors] = useState({});

  const researcher = {
    surname: "",
    forename: "",
    email: "",
    accreditedNumber: "",
  };
  const [researcherTeam, setResearcherTeam] = useState([
    { ...researcher },
    { ...researcher },
    { ...researcher },
  ]);

  const teamSize = researcherTeam.length - 1;
  const theme = useTheme();
  function getStyles(name, dataset, theme) {
    return {
      fontWeight:
        dataset.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }

  const updateResearcherToTeam = (action, id) => {
    if (action === "add")
      setResearcherTeam([...researcherTeam, { ...researcher }]);
    else {
      const temp = [...researcherTeam];
      temp.splice(id, 1);

      setResearcherTeam(temp);
    }
  };

  const handleTeamChange = (event, id) => {
    const { name, value } = event.target;
    const updatedTeam = [...researcherTeam];
    updatedTeam[id][name] = value;

    setResearcherTeam(updatedTeam);

    handleChange(event, true, "team", researcherTeam);
  };

  function addSoftware(event, value) {
    let softwares = project.softwares;
    const isSelected = event.target.checked;
    const index = softwares.indexOf(value);

    if (!isSelected && index >= 0) softwares.splice(index, 1);

    if (isSelected && index === -1) softwares.push(value);

    handleChange(event, true, "softwareRequired", softwares);
  }

  function handleChange(event, isObject, name, value) {
    if (!isObject) {
      name = event.target.name;
      value = event.target.value;
    }

    setProject((prevProject) => ({
      ...prevProject,
      [name]: value,
    }));
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    saveProject(project);
  };

  const handleCancel = (event) => {
    event.preventDefault();
    history.push("/Workspace/Projects");
  };

  return (
    <div style={{ padding: "25px" }}>
      <div>
        <Typography variant="h5" className={classes.heading}>
          Create new project
        </Typography>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleSubmit}
        >
          Submit for approval
        </Button>

        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          type="submit"
          form="project-form"
          onClick={handleSubmit}
        >
          Save changes
        </Button>

        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </div>
      <div className={classes.clearFloat}></div>

      <form onSubmit={handleSubmit} id="project-form">
        {errors.onSave && (
          <div className="alert alert-danger" role="alert">
            {errors.onSave}
          </div>
        )}

        <div>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              className={classes.expansionPanelHeading}
            >
              <Typography variant="h6">Key Information</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Title
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="title"
                    fullWidth
                    label="Title of research project"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.title}
                    name="title"
                    error={errors.title}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Project start
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="startDate"
                    type="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={(e) => handleChange(e, false)}
                    error={errors.startDate}
                    value={project.startDate}
                    name="startDate"
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Project end
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="endData"
                    type="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={(e) => handleChange(e, false)}
                    error={errors.endData}
                    value={project.endData}
                    name="endData"
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    AR number
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="arNumber"
                    fullWidth
                    label="If you have an Accredited Research number, please enter it here"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.arNumber}
                    name="arNumber"
                    error={errors.arNumber}
                  />
                  <div>
                    To become an Accredited Researcher, please complete the
                    Accredited Researcher application form available on
                    the&nbsp;
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href="https://www.statisticsauthority.gov.uk/about-the-authority/better-useofdata-statistics-and-research/betterdataaccess-research/better-useofdata-for-research-information-for-researchers/"
                    >
                      UK Statistics Authority website
                    </a>
                    .
                  </div>
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              className={classes.expansionPanelHeading}
            >
              <Typography variant="h6">Research description</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Theme
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="theme"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe the theme that best applies to your research project"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.theme}
                    name="theme"
                    error={errors.theme}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Abstract
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="Abstract"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Include a short decription of the project and its benefits, in no mpre than 100 words"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.abstract}
                    name="abstract"
                    error={errors.abstract}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Purpose
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="Purpose"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Provide a detailed description of the purpose for which the data is requested, describing the aims
                    of the study/research in no more than 500 words. Where research is part of a large programme please include deatils below."
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.purpose}
                    name="purpose"
                    error={errors.purpose}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Methodology
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="Methodology"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Provide details of the reserach protocol or methodolgy (e.g. data linkage or matching, web scraping etc) and 
                    how you intend to use the data, in no more than  1000 words. "
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.methodology}
                    name="methodology"
                    error={errors.methodology}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              className={classes.expansionPanelHeading}
            >
              <Typography variant="h6">Researcher Team</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Additional researchers on project
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Table
                    className={classes.table}
                    aria-label="customized table"
                  >
                    <TableHead>
                      <TableRow>
                        <StyledTableCell>Forename</StyledTableCell>
                        <StyledTableCell align="right">Surname</StyledTableCell>
                        <StyledTableCell align="right">Email</StyledTableCell>
                        <StyledTableCell align="right">
                          AR number
                        </StyledTableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {researcherTeam.map((row, idx) => (
                        <TableRow key={`teamMember-${idx}`}>
                          <StyledTableCell align="right">
                            {idx === teamSize && (
                              <div
                                style={{ position: "relative" }}
                                key="add-researcher"
                              >
                                <IconButton
                                  color="primary"
                                  aria-label="add researcher"
                                  onClick={(e) => updateResearcherToTeam("add")}
                                  style={{ position: "absolute", left: -40 }}
                                >
                                  <AddCircleOutlineOutlinedIcon fontSize="inherit" />
                                </IconButton>
                              </div>
                            )}
                            <TextField
                              id={`teamMember-${idx}-forename`}
                              onChange={(e) => handleTeamChange(e, idx)}
                              error={errors.forename}
                              value={row.forename}
                              name="forename"
                            />
                          </StyledTableCell>
                          <StyledTableCell align="right">
                            <TextField
                              id={`teamMember-${idx}-surname`}
                              onChange={(e) => handleTeamChange(e, idx)}
                              error={errors.surname}
                              value={row.surname}
                              name="surname"
                            />
                          </StyledTableCell>
                          <StyledTableCell align="right">
                            <TextField
                              id={`teamMember-${idx}-email`}
                              onChange={(e) => handleTeamChange(e, idx)}
                              error={errors.email}
                              value={row.email}
                              name="email"
                            />
                          </StyledTableCell>
                          <StyledTableCell align="right">
                            <TextField
                              id={`teamMember-${idx}-accreditedNumber`}
                              onChange={(e) => handleTeamChange(e, idx)}
                              error={errors.surname}
                              value={row.accreditedNumber}
                              name="accreditedNumber"
                            />

                            <div
                              style={{ position: "relative" }}
                              key="delete-researcher"
                            >
                              <IconButton
                                color="secondary"
                                aria-label="delete researcher"
                                onClick={(e) =>
                                  updateResearcherToTeam("delete", idx)
                                }
                                style={{
                                  position: "absolute",
                                  right: -40,
                                  top: -40,
                                }}
                              >
                                <RemoveCircleOutlineOutlinedIcon fontSize="inherit" />
                              </IconButton>
                            </div>
                          </StyledTableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                  <div>Please provide details of all members on team</div>
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              className={classes.expansionPanelHeading}
            >
              <Typography variant="h6">Research sponsor</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={3}>
                <Grid item xs>
                  <div style={{ color: "red" }}>
                    <b>NB</b> Only complete this section if you have been
                    commissioned to perform this research for another
                    organisation.
                  </div>
                </Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Organisation
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="orgName"
                    fullWidth
                    label="Sponsor organisation name"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.orgName}
                    name="orgName"
                    error={errors.orgName}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Organisation contact
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="orgContact"
                    fullWidth
                    label="Forename and surename or main organisational contact"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.orgContact}
                    name="orgContact"
                    error={errors.orgContact}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Contact address
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="orgContactAddress"
                    fullWidth
                    label="Address of organisational contact"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.orgContactAddress}
                    name="orgContactAddress"
                    error={errors.orgContactAddress}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Contact postcode
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="orgContactPostcode"
                    fullWidth
                    label="Postcode of organisational contact"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.orgContactPostcode}
                    name="orgContactPostcode"
                    error={errors.orgContactPostcode}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Contact telephone
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="orgContactTelephone"
                    fullWidth
                    label="Main telephone number of organisational contact"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.orgContactTelephone}
                    name="orgContactTelephone"
                    error={errors.orgContactTelephone}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Contact email
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="orgContactEmail"
                    fullWidth
                    label="Main email address of organisational contact"
                    margin="normal"
                    onChange={(e) => handleChange(e, false)}
                    value={project.orgContactEmail}
                    name="orgContactEmail"
                    error={errors.orgContactEmail}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              className={classes.expansionPanelHeading}
            >
              <Typography variant="h6">Data and software required</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Add dataset
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <Select
                      id="datasets"
                      multiple
                      value={project.datasets}
                      name="datasets"
                      onChange={(e) => handleChange(e, false)}
                      input={<Input id="select-multiple-chip" fullWidth />}
                      renderValue={(selected) => (
                        <div className={classes.chips}>
                          {selected.map((value) => (
                            <Chip
                              key={value}
                              label={value}
                              className={classes.chip}
                            />
                          ))}
                        </div>
                      )}
                    >
                      {dataSetList.map((name) => (
                        <MenuItem
                          key={name}
                          value={name}
                          style={getStyles(name, project.datasets, theme)}
                        >
                          {name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs></Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="additionalInfo"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="If you intend to link any of these data sources, please provide the following details:
                    1. description of the data sources to be linked. 
                    2. summary of the key variables
                    3. summary of the linking methodology
                    4. the justification of the linking."
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.additionalInfo}
                    name="additionalInfo"
                    error={errors.additionalInfo}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Software
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Table>
                    <TableBody>
                      {softwaresList.map((row, index) => {
                        const isItemSelected =
                          project.softwares.indexOf(row) !== -1;
                        const labelId = `enhanced-table-checkbox-${index}`;

                        return (
                          <TableRow
                            hover
                            onClick={(event) => addSoftware(event, row)}
                            role="checkbox"
                            key={row}
                            style={{ border: "1px solid black" }}
                          >
                            <TableCell padding="checkbox">
                              <Checkbox checked={isItemSelected} />
                            </TableCell>
                            <TableCell>{row}</TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              className={classes.expansionPanelHeading}
            >
              <Typography variant="h6">Ethics and Public Good</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Ethical approval
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="ethicsApproval"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="If you have an ethical approval from an ethical, Yes, please give details of ethical approval
                    body an any caveats, attaching any relevant documentation"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.ethicsApproval}
                    name="ethicsApproval"
                    error={errors.ethicsApproval}
                  />
                  <div>
                    If you do not have an ethical approval, you may wish to use
                    and submit a complete UK Statistics Authority Data Ethics
                    self-assesment form available on&nbsp;
                    <a href="https://www.statisticsauthority.gov.uk/about-the-authority/committees/nsdec/data-ethics/self-assessment-2/">
                      this website
                    </a>
                  </div>
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - policy
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="publicGoodPolicy"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how this reserach project will provide an evidence for public policy decision-making"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.publicGoodPolicy}
                    name="publicGoodPolicy"
                    error={errors.publicGoodPolicy}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - service
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="publicGoodService"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how thisreserach project will provide an evidence base for public service delivery"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.publicGoodService}
                    name="publicGoodService"
                    error={errors.publicGoodService}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - economy
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="publicGoodEconomy"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how this research project  will provide an evidence base for decisions which are likely
                    to sognificatly benefit the UK economy, society for qality of life of the people in UK"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.publicGoodEconomy}
                    name="publicGoodEconomy"
                    error={errors.publicGoodEconomy}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - statistics
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="publicGoodStatistics"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how this reseach project will replicate, validate or challenge official statistics"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.publicGoodStatistics}
                    name="publicGoodStatistics"
                    error={errors.publicGoodStatistics}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - research
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="publicGoodResearch"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how this reseach project will replicate, validate or challenge existing research"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.publicGoodResearch}
                    name="publicGoodResearch"
                    error={errors.publicGoodResearch}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - social
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="pubicGoodSocial"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how this reserach project will significantly extend understanding of social or economic trends or events 
                    by improving knowledge or challenging widely accepting analyses"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.pubicGoodSocial}
                    name="pubicGoodSocial"
                    error={errors.pubicGoodSocial}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item xs>
                  <Typography variant="subtitle1" className={classes.formLabel}>
                    Public good - coverage
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    id="publicGoodCoverage"
                    fullWidth
                    multiline
                    rows={5}
                    placeholder="Describe how this research project will improve the quality, coverage or presentation of existing statistical 
                    information"
                    onChange={(e) => handleChange(e, false)}
                    defaultValue={project.publicGoodCoverage}
                    name="publicGoodCoverage"
                    error={errors.publicGoodCoverage}
                  />
                </Grid>
                <Grid item xs={3}></Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      </form>
    </div>
  );
}

NewProject.propTypes = {
  saveProject: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  project: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    project: newProject,
  };
}

const mapDispatchToProps = {
  saveProject,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NewProject)
);
