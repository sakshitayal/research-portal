import React, { useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import {
  postUserLogin,
  addTTOP,
  VerifyToken,
  ConfirmSignIn,
} from "../../Redux/actions/userActions";
import FormControl from "@material-ui/core/FormControl";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import clsx from "clsx";
import FormHelperText from "@material-ui/core/FormHelperText";
import { ROUTES } from "../../Utils/routes";
import QRCode from "qrcode.react";

const useStyles = makeStyles((theme) => ({
  formAlignment: {
    border: "2px solid #9e9999",
    margin: "auto",
    width: "30%",
    textAlign: "center",
    minHeight: "300px",
    padding: 20,
    backgroundColor: "rgba(216, 216, 216, 0.14)",
  },
  textField: {
    marginTop: 20,
    width: 300,
    // boxShadow: "2px 2px 2px 2px rgba(0,0,0,0.12)",
  },
  button: {
    margin: 20,
    width: 200,
    fontSize: 18,
    boxShadow: "4px 4px rgba(0, 0, 0, 0.12)",
  },
  alert: {
    color: "red",
  },
}));

function Login({
  postUserLogin,
  addTTOP,
  VerifyToken,
  ConfirmSignIn,
  history,
  ...props
}) {
  const modal = {
    username: "",
    password: "",
    payload: "",
    qrCode: "",
    showConfirmation: false,
    challengeAnswer: "",
  };

  const classes = useStyles();
  const [user, setUser] = useState({ ...modal });
  const [saving, setSaving] = useState(false);
  const [errors, setErrors] = useState({});

  function handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    setUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  }

  const setQRCode = (payload) => {
    addTTOP(payload)
      .then((value) => {
        setUser((prevUser) => ({
          ...prevUser,
          qrCode: value,
        }));
      })
      .catch((error) => {
        setErrors({ onSave: error.message });
      });
  };

  function formIsValid() {
    const { username, password } = user;
    const errors = {};

    if (!username) errors.username = "username is required.";
    if (!password) errors.password = "password is required";

    setErrors(errors);
    // Form is valid if the errors object still has no properties
    return Object.keys(errors).length === 0;
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setSaving(true);
    if (user.showConfirmation && user.payload.challengeName === "MFA_SETUP") {
      VerifyToken(user.payload, user.challengeAnswer)
        .then(() => {})
        .catch((error) => {
          setSaving(false);
          setErrors({ onSave: error.message });
        });
    } else if (
      user.showConfirmation &&
      user.payload.challengeName === "SOFTWARE_TOKEN_MFA"
    ) {
      ConfirmSignIn(user.payload, user.challengeAnswer)
        .then(() => {})
        .catch((error) => {
          setSaving(false);
          setErrors({ onSave: error.message });
        });
    } else {
      if (!formIsValid()) return;
      postUserLogin(user)
        .then((payload) => {
          if (payload.challengeName === "NEW_PASSWORD_REQUIRED") {
            history.push(ROUTES.NEWPASSWORD, {
              username: user.username,
              password: user.password,
            });
          } else {
            setUser((prevUser) => ({
              ...prevUser,
              payload: payload,
              showConfirmation: true,
            }));
            setSaving(false);
            if (payload.challengeName === "MFA_SETUP") setQRCode(payload);
          }
        })
        .catch((error) => {
          setSaving(false);
          setErrors({ onSave: error.message });
        });
    }

    console.log(errors.onSave);
  };

  const handleCancel = (event) => {
    event.preventDefault();
    history.push("/");
  };

  return (
    <div style={{ padding: "30px" }}>
      <form id="login-form" className={classes.formAlignment}>
        {user.showConfirmation && (
          <>
            <h2>Multi-factor authentication</h2>
            <hr />
            {errors.onSave && (
              <div className={classes.alert}>{errors.onSave}</div>
            )}

            {user.qrCode && (
              <>
                <h4>
                  Use below qr-code to set multi factor autentication on your
                  mobile app, then type the OTP to login.
                </h4>
                <div>
                  <QRCode value={user.qrCode} />
                </div>
                <br />
              </>
            )}

            <TextField
              variant="outlined"
              id="otp"
              label="OTP"
              onChange={handleChange}
              value={user.challengeAnswer}
              name="challengeAnswer"
              className={classes.textField}
              error={errors.challengeAnswer}
              helperText={errors.challengeAnswer ? errors.challengeAnswer : ""}
            />
          </>
        )}
        {!user.showConfirmation && (
          <>
            <h2>Login to Researcher Portal</h2>
            <hr />
            {errors.onSave && (
              <div className={classes.alert}>{errors.onSave}</div>
            )}

            <div>
              <TextField
                variant="outlined"
                id="login-username"
                label="username"
                onChange={handleChange}
                value={user.username}
                name="username"
                className={classes.textField}
                error={errors.username}
                helperText={errors.username ? errors.username : ""}
              />

              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
                error={errors.password}
              >
                <InputLabel htmlFor="outlined-adornment-password">
                  Password
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={"password"}
                  value={user.password}
                  onChange={handleChange}
                  name="password"
                  labelWidth={70}
                />
                <FormHelperText>{errors.password}</FormHelperText>
              </FormControl>
            </div>
          </>
        )}

        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          type="submit"
          form="project-form"
          onClick={handleSubmit}
          disabled={saving}
        >
          {saving ? "Logging..." : "Login"}
        </Button>

        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </form>
    </div>
  );
}

Login.propTypes = {
  postUserLogin: PropTypes.func.isRequired,
  addTTOP: PropTypes.func.isRequired,
  VerifyToken: PropTypes.func.isRequired,
  ConfirmSignIn: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { user } = state;

  return {
    user,
  };
}

const mapDispatchToProps = {
  postUserLogin,
  addTTOP,
  VerifyToken,
  ConfirmSignIn,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
