import React, { useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { completeNewPassword } from "../../Redux/actions/userActions";
import FormControl from "@material-ui/core/FormControl";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import clsx from "clsx";
import FormHelperText from "@material-ui/core/FormHelperText";

import { cognitoPasswordRegEx } from "../../Utils/utils";
import { ROUTES } from "../../Utils/routes";

const useStyles = makeStyles((theme) => ({
  formAlignment: {
    border: "2px solid #9e9999",
    margin: "auto",
    width: "30%",
    textAlign: "center",
    minHeight: "300px",
    padding: 20,
    backgroundColor: "rgba(216, 216, 216, 0.14)",
  },
  textField: {
    marginTop: 20,
    width: 300,
    // boxShadow: "2px 2px 2px 2px rgba(0,0,0,0.12)",
  },
  button: {
    margin: 20,
    width: 200,
    fontSize: 18,
    boxShadow: "4px 4px rgba(0, 0, 0, 0.12)",
  },
  alert: {
    color: "red",
  },
}));

function NewPassword({ completeNewPassword, history, ...props }) {
  const classes = useStyles();
  const changePasswordObj = {
    username: history.location.state.username,
    password: history.location.state.password,
    newPassword: "",
    confirmPassword: "",
  };

  const [user, setUser] = useState({ ...changePasswordObj });
  const [saving, setSaving] = useState(false);
  const [errors, setErrors] = useState({});

  function handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    setUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  }

  function formIsValid() {
    const { newPassword, confirmPassword } = user;
    const errors = {};

    if (!confirmPassword)
      errors.confirmPassword = "confirmPassword is required.";
    else if (confirmPassword !== newPassword)
      errors.confirmPassword = "password and confirm password do not match";

    if (!newPassword) errors.newPassword = "password is required";
    else if (!newPassword.match(cognitoPasswordRegEx()))
      errors.newPassword = "password is not valid";

    setErrors(errors);
    return Object.keys(errors).length === 0;
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!formIsValid()) return;
    setSaving(true);
    completeNewPassword(user)
      .then(() => {
        history.push(ROUTES.LOGIN);
      })
      .catch((error) => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });

    console.log(errors.onSave);
  };

  const handleCancel = (event) => {
    event.preventDefault();
    history.push("/");
  };

  return (
    <div style={{ padding: "30px" }}>
      <form id="login-form" className={classes.formAlignment}>
        <h2>Change Password</h2>
        <hr />
        {errors.onSave && <div className={classes.alert}>{errors.onSave}</div>}

        <div>
          <FormControl
            className={clsx(classes.margin, classes.textField)}
            variant="outlined"
            error={errors.newPassword}
          >
            <InputLabel htmlFor="outlined-adornment-password">
              Password
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={"password"}
              value={user.newPassword}
              onChange={handleChange}
              name="newPassword"
              labelWidth={70}
            />
            <FormHelperText>{errors.newPassword}</FormHelperText>
          </FormControl>

          <FormControl
            className={clsx(classes.margin, classes.textField)}
            variant="outlined"
            error={errors.confirmPassword}
          >
            <InputLabel htmlFor="outlined-adornment-password">
              Confirm Password
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={"password"}
              value={user.confirmPassword}
              onChange={handleChange}
              name="confirmPassword"
              labelWidth={70}
            />
            <FormHelperText>{errors.confirmPassword}</FormHelperText>
          </FormControl>
        </div>

        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          type="submit"
          form="project-form"
          onClick={handleSubmit}
          disabled={saving}
        >
          {saving ? "updating..." : "update"}
        </Button>

        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </form>
    </div>
  );
}

NewPassword.propTypes = {
  completeNewPassword: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { user } = state;

  return {
    user,
  };
}

const mapDispatchToProps = {
  completeNewPassword,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NewPassword)
);
