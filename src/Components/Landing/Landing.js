import React from "react";
import Typography from "@material-ui/core/Typography";

function Landing() {
  return (
    <div style={{ padding: "25px" }}>
      <Typography paragraph>Logged In</Typography>
      <Typography paragraph>About SMRP</Typography>
      <Typography paragraph>Getting started</Typography>
      <Typography paragraph>Site map and terminology</Typography>
      <Typography paragraph>Glossary</Typography>
      <Typography paragraph>SMRP documentation</Typography>
      <Typography paragraph>
        Quick links to publicaly available datasets
      </Typography>
      <Typography paragraph>Privacy statments</Typography>
    </div>
  );
}

export default Landing;
