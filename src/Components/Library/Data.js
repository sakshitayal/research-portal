import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Spinner from "../../Common/Spinner";
import { connect } from "react-redux";
import * as datasetActions from "../../Redux/actions/dataSetAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import CustomTable from "../../Common/table";

const useStyles = makeStyles((theme) => ({
  detailGrid: { height: 50 },
  heading: { fontWeight: 700 },
}));

const dataSetColumns = [
  { id: "title", label: "Title" },
  { id: "cases", label: "Cases" },
  { id: "released", label: "Released" },
];

const documentationColumns = [
  { id: "name", label: "Name" },
  { id: "description", label: "Description" },
  { id: "format", label: "Format" },
];

const variableColumns = [
  { id: "name", label: "Name" },
  { id: "description", label: "Description" },
  { id: "type", label: "Type" },
];

function Data(props) {
  const classes = useStyles();
  const { dataSets, actions } = props;

  useEffect(() => {
    if (dataSets.length === 0) {
      actions.loadDataSets().catch((error) => {
        alert("Loading datasets failed" + error);
      });
    }
  }, []);

  const onRowClick = (event, id) => {
    debugger;
    let output = [];
    const data = dataSets[id];

    output.push(
      <Paper style={{ padding: "10px" }} key="0">
        <Grid container>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Title
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["title"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Released
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["released"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Abstract
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <br />
            <Typography variant="subtitle1" gutterBottom>
              {data["abstract"]}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Documentation
            </Typography>

            <CustomTable
              columns={documentationColumns}
              rows={data["documentation"]}
              rowsPerPageArr={[5, 10, 15]}
            />
          </Grid>
          <Grid item xs={12}>
            <br />
            <br />
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Variables
            </Typography>

            <CustomTable
              columns={variableColumns}
              rows={data["variables"]}
              rowsPerPageArr={[5, 10, 15]}
            />
          </Grid>
        </Grid>
      </Paper>
    );

    ReactDOM.render(output, document.getElementById("dataset-detail"));
  };

  return (
    <Grid container>
      <div style={{ padding: "15px" }}>
        <Typography variant="h4" gutterBottom>
          Browse Datasets
        </Typography>
      </div>

      <Grid item xs={12} style={{ borderTop: "1px black solid" }}>
        <Grid container justify="center" spacing={0}>
          <Grid key={0} item xs={6} style={{ borderRight: "1px black solid" }}>
            {props.loading ? (
              <Spinner />
            ) : (
              <CustomTable
                columns={dataSetColumns}
                rows={dataSets}
                rowsPerPageArr={[20, 25, 30]}
                onRowClick={onRowClick}
              />
            )}
          </Grid>
          <Grid
            key={1}
            item
            xs={6}
            style={{ borderLeft: "1px black solid" }}
            id="dataset-detail"
          ></Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

Data.propTypes = {
  dataSets: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    dataSets: state.dataSets,
    loading: state.apiCallsInProgress > 0,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadDataSets: bindActionCreators(datasetActions.loadDataSets, dispatch),
    },
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Data));
