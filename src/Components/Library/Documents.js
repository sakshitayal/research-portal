import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Spinner from "../../Common/Spinner";
import { connect } from "react-redux";
import * as documentActions from "../../Redux/actions/documentAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import CustomTable from "../../Common/table";

const useStyles = makeStyles((theme) => ({
  detailGrid: { height: 50 },
  heading: { fontWeight: 700 },
}));

const documentColumns = [
  { id: "title", label: "Title" },
  { id: "dataSet", label: "DataSet" },
];

function Documents(props) {
  const classes = useStyles();
  const { documents, actions } = props;

  useEffect(() => {
    if (documents.length === 0) {
      actions.loadDocuments().catch((error) => {
        alert("Loading docmets failed" + error);
      });
    }
  }, []);

  const onRowClick = (event, id) => {
    let output = [];
    const data = documents[id];

    output.push(
      <Paper style={{ padding: "10px" }} key="0">
        <Grid container>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Title
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["title"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Format
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["format"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Dataset
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["dataSet"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Author(s)
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["author"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Published date
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["publishedDate"]}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="subtitle1"
              gutterBottom
              className={classes.heading}
            >
              Abstract
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1" gutterBottom>
              {data["abstract"]}
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    );

    ReactDOM.render(output, document.getElementById("document-detail"));
  };

  return (
    <Grid container>
      <div style={{ padding: "15px" }}>
        <Typography variant="h4" gutterBottom>
          Browse Documents
        </Typography>
      </div>

      <Grid item xs={12} style={{ borderTop: "1px black solid" }}>
        <Grid container justify="center" spacing={0}>
          <Grid key={0} item xs={6} style={{ borderRight: "1px black solid" }}>
            {props.loading ? (
              <Spinner />
            ) : (
              <CustomTable
                columns={documentColumns}
                rows={documents}
                rowsPerPageArr={[20, 25, 30]}
                onRowClick={onRowClick}
              />
            )}
          </Grid>
          <Grid
            key={1}
            item
            xs={6}
            style={{ borderLeft: "1px black solid" }}
            id="document-detail"
          ></Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

Documents.propTypes = {
  documents: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    documents: state.documents,
    loading: state.apiCallsInProgress > 0,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadDocuments: bindActionCreators(
        documentActions.loadDocuments,
        dispatch
      ),
    },
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Documents)
);
