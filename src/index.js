import React from "react";
import ReactDOM from "react-dom";
//import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";

import configureStore from "./Redux/configureStore";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

const store = configureStore();

// Render the app
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
