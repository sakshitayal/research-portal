export const CREATE_PRODUCT_SUCCESS = "CREATE_PRODUCT_SUCCESS";
export const UPDATE_PRODUCT_SUCCESS = "UPDATE_PRODUCT_SUCCESS";
export const LOAD_PRODUCTS_SUCCESS = "LOAD_PRODUCTS_SUCCESS";
export const LOAD_RESOURCES_SUCCESS = "LOAD_RESOURCES_SUCCESS";
export const LOAD_DATASETS_SUCCESS = "LOAD_DATASETS_SUCCESS";
export const LOAD_DOCUMENTS_SUCCESS = "LOAD_DOCUMENTS_SUCCESS";
export const LOAD_VARIABLES_SUCCESS = "LOAD_VARIABLES_SUCCESS";
export const CREATE_PROJECT_SUCCESS = "CREATE_PROJECT_SUCCESS";
export const UPDATE_PROJECT_SUCCESS = "UPDATE_PROJECT_SUCCESS";
export const LOAD_PROJECTS_SUCCESS = "LOAD_PROJECTS_SUCCESS";
export const BEGIN_API_CALL = "BEGIN_API_CALL";
export const API_CALL_ERROR = "API_CALL_ERROR";
export const GET_USER_LOGIN_SUCCESS = "GET_USER_LOGIN_SUCCESS";
export const GET_USER_LOGIN_CANCEL = "GET_USER_LOGIN_CANCEL";
export const GET_USER_LOGIN_FAILURE = "GET_USER_LOGIN_FAILURE";
export const POST_USER_LOGIN_CANCEL = "POST_USER_LOGIN_CANCEL";
export const POST_USER_LOGIN_SUCCESS = "POST_USER_LOGIN_SUCCESS";
export const POST_USER_LOGIN_FAILURE = "POST_USER_LOGIN_FAILURE";
export const USER_LOGOUT_SUCCESS = "USER_LOGOUT_SUCCESS";
export const POST_FORGOT_PASSWORD_CANCEL = "POST_FORGOT_PASSWORD_CANCEL";
export const POST_FORGOT_PASSWORD_SUCCESS = "POST_FORGOT_PASSWORD_SUCCESS";
export const POST_FORGOT_PASSWORD_FAILURE = "POST_FORGOT_PASSWORD_FAILURE";
export const POST_FORGOT_PASSWORD_RESET_CANCEL =
  "POST_FORGOT_PASSWORD_RESET_CANCEL";
export const POST_FORGOT_PASSWORD_RESET_SUCCESS =
  "POST_FORGOT_PASSWORD_RESET_SUCCESS";
export const POST_FORGOT_PASSWORD_RESET_FAILURE =
  "POST_FORGOT_PASSWORD_RESET_FAILURE";
