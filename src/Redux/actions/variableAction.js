import * as types from "./actionTypes";
import * as variablesApi from "../../Api/variablesApi";

export function loadVariablesSuccess(variables) {
  return { type: types.LOAD_VARIABLES_SUCCESS, variables };
}

export function loadVariables() {
  return function(dispatch) {
    return variablesApi
      .getvariables()
      .then(variables => {
        dispatch(loadVariablesSuccess(variables));
      })
      .catch(error => {
        throw error;
      });
  };
}
