import * as types from "./actionTypes";
import * as dataSetApi from "../../Api/dataSetApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function loadDocumentsSuccess(documents) {
  return { type: types.LOAD_DOCUMENTS_SUCCESS, documents };
}

export function loadDocuments() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return dataSetApi
      .getdocuments()
      .then((documents) => {
        dispatch(loadDocumentsSuccess(documents));
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}
