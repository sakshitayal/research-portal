import * as types from "./actionTypes";
import * as dataSetApi from "../../Api/dataSetApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function loadDataSetsSuccess(dataSets) {
  return { type: types.LOAD_DATASETS_SUCCESS, dataSets };
}

export function loadDataSets() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return dataSetApi
      .getdataSets()
      .then((dataSets) => {
        dispatch(loadDataSetsSuccess(dataSets));
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}
