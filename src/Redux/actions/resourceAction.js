import * as types from "./actionTypes";
import * as resourcesApi from "../../Api/resourceApi";

export function loadResourcesSuccess(resources) {
  return { type: types.LOAD_RESOURCES_SUCCESS, resources };
}

export function loadResources() {
  return function(dispatch) {
    return resourcesApi
      .getResources()
      .then(resources => {
        dispatch(loadResourcesSuccess(resources));
      })
      .catch(error => {
        throw error;
      });
  };
}
