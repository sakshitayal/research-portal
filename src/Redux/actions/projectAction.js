import * as types from "./actionTypes";
import * as projectApi from "../../Api/projectApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function createProjectSuccess(project) {
  return { type: types.CREATE_PROJECT_SUCCESS, project };
}

export function updateProjectSuccess(project) {
  return { type: types.UPDATE_PROJECT_SUCCESS, project };
}

export function saveProject(project) {
  return function(dispatch, getState) {
    dispatch(beginApiCall());
    return projectApi
      .saveProject(project)
      .then(savedProject => {
        project.id
          ? dispatch(updateProjectSuccess(savedProject))
          : dispatch(createProjectSuccess(savedProject));
      })
      .catch(error => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function loadProjectSuccess(projects) {
  return { type: types.LOAD_PROJECTS_SUCCESS, projects };
}

export function loadProjects() {
  return function(dispatch) {
    dispatch(beginApiCall());
    return projectApi
      .getProjects()
      .then(projects => {
        dispatch(loadProjectSuccess(projects));
      })
      .catch(error => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}
