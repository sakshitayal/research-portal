import * as types from "./actionTypes";
import authentication from "../../Utils/authentication";
import {
  getLocalStorage,
  setLocalStorage,
  removeLocalStorage,
} from "../../Utils/utils";
import { ROUTES } from "../../Utils/routes";
import { DEFAULT_USER_PREFS } from "../../Utils/localStorage";
import { beginApiCall, apiCallError } from "./apiStatusActions";
import { push } from "connected-react-router";

export function getUserLogin() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .getCurrentUser()
      .then((payload) => {
        const { signInUserSession } = payload;
        const { idToken } = signInUserSession;
        const { jwtToken } = idToken;
        const authPayload = idToken.payload;
        let authExpireTime = JSON.stringify(
          authPayload.exp + new Date().getTime()
        );
        const userLocalData = {
          token: jwtToken,
          email: authPayload.email,
          authTime: authPayload.auth_time,
          authExpireTime: authExpireTime,
        };
        dispatch(getUserLoginSuccess(userLocalData));
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        push(ROUTES.LOGIN);
        throw error;
      });
  };
}

export function getUserLoginSuccess(payload) {
  return { type: types.GET_USER_LOGIN_SUCCESS, payload };
}

export function postUserLogin(user) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .login(user)
      .then((payload) => {
        return payload;
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function postUserLoginSuccess(payload) {
  return { type: types.POST_USER_LOGIN_SUCCESS, payload };
}

export function userLogout() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .logout()
      .then((status) => {
        removeLocalStorage("user");
        removeLocalStorage("userPrefs");
        dispatch(userLogoutSuccess());
        dispatch(push(ROUTES.LOGIN));
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function userLogoutSuccess() {
  return { type: types.USER_LOGOUT_SUCCESS };
}

export function completeNewPassword(user) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .completeNewPassword(user)
      .then((payload) => {
        return payload;
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function addTTOP(payload) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .addTTOP(payload)
      .then((code) => {
        return code;
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function VerifyToken(payload, challengeAnswer) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .verifyTotpToken(payload, challengeAnswer)
      .then((user) => {
        authentication.setPreferredMFA(payload);
        HandleSignInSuccess(user, dispatch);
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

export function ConfirmSignIn(user, challengeAnswer) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return authentication
      .confirmSignIn(user, challengeAnswer)
      .then((payload) => {
        const { signInUserSession } = payload;
        HandleSignInSuccess(signInUserSession, dispatch);
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}

function HandleSignInSuccess(signInUserSession, dispatch) {
  const { idToken } = signInUserSession;
  const { jwtToken } = idToken;
  const authPayload = idToken.payload;
  let authExpireTime = JSON.stringify(authPayload.exp + new Date().getTime());
  const userLocalData = {
    token: jwtToken,
    email: authPayload.email,
    authTime: authPayload.auth_time,
    authExpireTime: authExpireTime,
  };
  setLocalStorage("user", userLocalData);

  if (!getLocalStorage("userPrefs")) {
    setLocalStorage("userPrefs", DEFAULT_USER_PREFS);
  }
  const params = new URLSearchParams(window.location.search);

  dispatch(postUserLoginSuccess(userLocalData));
  dispatch(push(params.get("redirect") || ROUTES.LANDING));
}
