import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function resourceReducer(
  state = initialState.resources,
  action
) {
  switch (action.type) {
    case types.LOAD_RESOURCES_SUCCESS:
      return action.resources;
    default:
      return state;
  }
}
