import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function dataSetReducer(state = initialState.dataSets, action) {
  switch (action.type) {
    case types.LOAD_DATASETS_SUCCESS:
      return action.dataSets;
    default:
      return state;
  }
}
