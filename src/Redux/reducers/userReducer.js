import * as userState from "../actions/actionTypes";
import initialState from "./initialState";

export default function userReducer(state = initialState.user, action) {
  switch (action.type) {
    case userState.GET_USER_LOGIN_SUCCESS:
    case userState.POST_USER_LOGIN_SUCCESS:
      return { ...state, ...action.payload };
    case userState.POST_FORGOT_PASSWORD_SUCCESS:
      return action.payload;
    case userState.POST_USER_LOGIN_CANCEL:
    case userState.GET_USER_LOGIN_CANCEL:
    case userState.USER_LOGOUT_SUCCESS:
      return null;
    default:
      return state;
  }
}
