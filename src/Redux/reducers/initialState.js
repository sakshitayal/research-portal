export default {
  products: [],
  resources: [],
  dataSets: [],
  variables: [],
  projects: [],
  documents: [],
  apiCallsInProgress: 0,
  user: null,
};
