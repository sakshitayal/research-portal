import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function variableReducer(
  state = initialState.variables,
  action
) {
  switch (action.type) {
    case types.LOAD_VARIABLES_SUCCESS:
      return action.variables;
    default:
      return state;
  }
}
