import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import products from "./productReducer";
import resources from "./resourceReducer";
import dataSets from "./dataSetReducer";
import documents from "./documentsReducer";
import variables from "./variableReducer";
import projects from "./projectReducer";
import apiCallsInProgress from "./apiStatusReducer";
import user from "./userReducer";

const rootReducer = (history) => {
  return combineReducers({
    router: connectRouter(history),
    products,
    resources,
    dataSets,
    variables,
    projects,
    apiCallsInProgress,
    user,
    documents,
  });
};

export default rootReducer;
