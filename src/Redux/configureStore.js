import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";

export const history = createBrowserHistory();

const createRootReducer = (history) => {
  return rootReducer(history);
};

export default function configureStore(initialState) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //add support for redux dev tools

  const middleware =
    process.env.NODE_ENV !== "production"
      ? [
          require("redux-immutable-state-invariant").default(),
          thunk,
          routerMiddleware(history),
        ]
      : [thunk, routerMiddleware(history)];

  if (process.env.NODE_ENV === "development") {
    middleware.push(logger);
  }

  return createStore(
    createRootReducer(history),
    initialState,
    composeEnhancers(applyMiddleware(...middleware))
  );
}
