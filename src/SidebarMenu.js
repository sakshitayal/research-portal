import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import _ from "lodash";

const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#0c0c0c",
  },
  toolbar: theme.mixins.toolbar,
  drawerListItem: {
    marginTop: 8,
    marginBottom: 8,
    color: "#fafafa",
    textDecoration: "none",
    fontSize: 17,
    fontWeight: 700,
  },
  subMenuList: {
    marginLeft: 50,
    padding: 0,
  },
  subMenuListItem: {
    textDecoration: "none",
    color: "#fafafa",
    fontSize: "1rem",
  },
}));

const mainSideMenu = [
  {
    key: "library",
    text: "Library",
    subMenu: [
      { key: "Data", text: "Data" },
      { key: "Documents", text: "Documents" },
    ],
  },
  {
    key: "workspace",
    text: "My Workspace",
    subMenu: [{ key: "Projects", text: "Projects" }],
  },
  {
    key: "access",
    text: "Access",
    subMenu: [{ key: "Projects", text: "Projects" }],
  },
];

function SidebarMenu(props) {
  const classes = useStyles();

  // We will get the full pathname of the form /app/subApp e.g. '/workspace/data' and here we only care
  // which app we are in.  The top menu of the app itself will then highight the sub app.
  let app = _.split(props.location.pathname, "/")[0];

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{ paper: classes.drawerPaper }}
    >
      <div className={classes.toolbar} />
      <List>
        {mainSideMenu.map((item, index) => (
          <React.Fragment key={index}>
            {item.subMenu.length > 0 ? (
              <>
                <ListItem button key={item.key}>
                  <span className={classes.drawerListItem}>{item.text}</span>
                </ListItem>

                <List className={classes.subMenuList}>
                  {item.subMenu.map((val) => (
                    <ListItem button key={val.key}>
                      <Link
                        className={classes.subMenuListItem}
                        to={`${app}/${item.key}/${val.key}`}
                      >
                        <span>{val.text}</span>
                      </Link>
                    </ListItem>
                  ))}
                </List>
              </>
            ) : (
              <ListItem button key={item.key}>
                <Link className={classes.drawerListItem} to={`/${item.key}`}>
                  <span>{item.text}</span>
                </Link>
              </ListItem>
            )}
          </React.Fragment>
        ))}
      </List>
    </Drawer>
  );
}

SidebarMenu.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(SidebarMenu);
