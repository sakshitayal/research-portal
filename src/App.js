import React from "react";
import { connect } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { Route, Switch } from "react-router";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { bindActionCreators } from "redux";
import { history } from "./Redux/configureStore";

import { makeStyles } from "@material-ui/core/styles";

import {
  userIsAuthenticatedRedir,
  userIsNotAuthenticatedRedir,
} from "./Utils/user";

import * as userActions from "./Redux/actions/userActions";
import Header from "./Header";
import MainSection from "./MainSection";
import { ROUTES } from "./Utils/routes";
import LibData from "./Components/Library/Data";
import LibDocuments from "./Components/Library/Documents";
import WSDashboard from "./Components/Workspace/Dashboard";
import WSDataProductBuilder from "./Components/Workspace/DataProductBuilder";
import WSDataResources from "./Components/Workspace/DataResources";
import WSProjects from "./Components/Workspace/Projects";
import BuildDataBuilder from "./Components/Workspace/BuildDataProduct1";
import newProject from "./Components/Workspace/NewProject";
import Login from "./Components/Login/Login";
import NewPassword from "./Components/NewPassword/NewPassword";
import Landing from "./Components/Landing/Landing";

import "./App.css";

const unAuthChain = compose(withRouter);
const authChain = compose(withRouter, userIsAuthenticatedRedir);
const LoginUnAuth = unAuthChain(userIsNotAuthenticatedRedir(Login));
const LandingAuth = authChain(userIsAuthenticatedRedir(Landing));
const LibDataAuth = authChain(userIsAuthenticatedRedir(LibData));
const LibDocumentsAuth = authChain(userIsAuthenticatedRedir(LibDocuments));
const WSDashboardAuth = authChain(userIsAuthenticatedRedir(WSDashboard));
const WSDataProductBuilderAuth = authChain(
  userIsAuthenticatedRedir(WSDataProductBuilder)
);
const WSDataResourcesAuth = authChain(
  userIsAuthenticatedRedir(WSDataResources)
);
const WSProjectsAuth = authChain(userIsAuthenticatedRedir(WSProjects));
const BuildDataBuilderAuth = authChain(
  userIsAuthenticatedRedir(BuildDataBuilder)
);
const newProjectAuth = authChain(userIsAuthenticatedRedir(newProject));

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "black",
  },
  content: {
    flexGrow: 1,
  },
  toolbar: theme.mixins.toolbar,
  loginLink: {
    float: "right",
    marginTop: 14,
  },
  loginAnchor: {
    textDecoration: "none",
    fontSize: 20,
    fontWeight: 700,
    color: "white",
  },
  loginIcon: {
    fontSize: "2em",
  },
  loginSpan: {
    verticalAlign: "super",
  },
  headerLogo: {
    float: "left",
    margin: 5,
  },
  displayBlock: { display: "block" },
}));

function App(props) {
  const classes = useStyles();

  return (
    <ConnectedRouter history={history}>
      <React.Fragment>
        <div className={classes.root}>
          <Header />
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Switch>
              <Route exact path={ROUTES.ROOT} component={MainSection} />
              <Route path={ROUTES.LOGIN} component={LoginUnAuth} />
              <Route path={ROUTES.NEWPASSWORD} component={NewPassword} />
              <Route path={ROUTES.LANDING} component={LandingAuth} />
              <Route path={ROUTES.DATA} component={LibDataAuth} />
              <Route path={ROUTES.DOCUMENTS} component={LibDocumentsAuth} />
              <Route path={ROUTES.DASHBOARD} component={WSDashboardAuth} />
              <Route
                path={ROUTES.DATAPRODUCTBUILDER}
                component={WSDataProductBuilderAuth}
              />
              <Route
                path={ROUTES.DATARESOURCES}
                component={WSDataResourcesAuth}
              />
              <Route path={ROUTES.PROJECTS} component={WSProjectsAuth} />
              <Route
                path={ROUTES.BUILDDATAPRODUCT1}
                component={BuildDataBuilderAuth}
              />
              <Route path={ROUTES.NEWPROJECT} component={newProjectAuth} />
            </Switch>
          </main>
        </div>
      </React.Fragment>
    </ConnectedRouter>
  );
}

function mapStateToProps(state) {
  const { user } = state;

  return {
    user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      getUserLogin: (url) =>
        bindActionCreators(userActions.getUserLogin(url), dispatch),
      userLogout: () => bindActionCreators(userActions.userLogout(), dispatch),
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
